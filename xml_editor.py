import sys
import xml.etree.ElementTree as Et
from enum import Enum


"""
The XMLEditor class loads an XML file into memory given the file's path.
The purpose of this class is to be able to load, edit and store XML documents.
"""


class XMLEditor(object):

    """
    A simple ENUM to store the correct number of arguments needed when
    invoking methods.
    """
    class ARGUMENTS(Enum):
        EDIT_TAG_VALUE_NO_ARGUMENTS = 6

    """
    Constructor to define object-level properties.
    """
    def __init__(self, path):
        self.my_tree = Et.parse(path)
        self.my_root = self.my_tree.getroot()

    """
    Helper method for edit_tag_value method.
    """
    def recursively_edit_tag_value(self, child, key, value):
        if len(child) == 0:
            if child.tag == key:
                child.text = value
                return True
        else:
            for element in child:
                return self.recursively_edit_tag_value(element, key, value)

    """
    Iterate over each element and check that it contains a certain attribute
    which has a certain value. If true we consider each child and
    recursively drill down till we get to a single child that has a certain
    tag. If such a child exists we update the value and return.
    """
    def edit_tag_value(self, parent_id, parent_attribute,
                       parent_value, key, value):
        for element in self.my_root.iter(parent_id):
            if parent_attribute in element.attrib:
                if element.attrib[parent_attribute] == parent_value:
                    for child in element:
                        if self.recursively_edit_tag_value(
                                child, key, value
                        ) is True:
                            return
                    break

    """
    Save the new document given a path.
    """
    def save_xml_file(self, path):
        self.my_tree.write(path)


if __name__ == "__main__":
    """
    The current setup intends to overwrite the original document.
    """
    args = sys.argv[1:]
    if len(args) == XMLEditor.ARGUMENTS.EDIT_TAG_VALUE_NO_ARGUMENTS.value:
        path_to_xml = args[0]

        parent_name = args[1]
        parent_name_attribute = args[2]
        parent_name_value = args[3]

        tag = args[4]
        new_value = args[5]

        xml_editor = XMLEditor(path_to_xml)
        xml_editor.edit_tag_value(parent_name, parent_name_attribute,
                                  parent_name_value, tag, new_value)
        xml_editor.save_xml_file(path_to_xml)
    else:
        raise ValueError(
            "Usage: python xml_editor.py <path_to_xml_file> "
            + "<element> <element attribute> <element attribute value> "
            + "<tag> <new value>"
        )
